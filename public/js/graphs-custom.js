    var datasets = [];
    var doughnutColors = ["#4a6ba0", "#45a79a", "#3a3263", "#d1d82a", "#857166", "#60bbda"];

    thrivingQA['answers'].forEach(function (answer, index) {

      var data = {
        data: [answer, 5 - answer],
        backgroundColor: [
          doughnutColors[answer],
          "#ecedf0"
        ],
        hoverBorderColor: [
          "#fff",
          "#fff"
        ],

        label: thrivingQA['questions'][index]
      };
      datasets.push(data);
    });

    //thriving-chart
    var getThrivingChart = document.getElementById("thriving-chart").getContext("2d");                                                                                                                                           
    var config = {
      type: 'doughnut',
      data: {
        datasets: datasets,
        labels: ['', '']
      },
      options: {
        segmentShowStroke: false,
        segmentStrokeWidth: 10,
        cutoutPercentage: 30,
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              return thrivingQA['questions'][tooltipItem.datasetIndex];
            }
          }
        },
        legend: {
          display: false
        }
      }
    };

    //thriving-chart
    var thrivingChart = new Chart(getThrivingChart, config);

    var data = {
//		labels: ["Skill#1", "Skill#2", "Skill#3", "Skill#4", "Skill#5"],
      labels: skillsQA['questions'],
//    display: false,
      datasets: [
        {
          label: "You",
          backgroundColor: "#3a3b74",
          borderWidth: 0,
          hoverBackgroundColor: "#3a3b74",
          data: skillsQA['answers']
        }
//			{
//				label: "Your Manager",
//				backgroundColor: "#3fa598",
//				borderWidth: 0,
//				hoverBackgroundColor: "#3fa598",
//				data: [1,5,4.2,1.7,4.5],
//			},
//			{
//				label: "Your Team",
//				backgroundColor: "#c6c1bb",
//				borderWidth: 0,
//				hoverBackgroundColor: "#c6c1bb",
//				data: [5,6,2,4,1.5],
//			},
//			{
//				label: "Your Multi-Raters",
//				backgroundColor: "#476aa4",
//				borderWidth: 0,
//				hoverBackgroundColor: "#476aa4",
//				data: [1,5,4.2,1.7,4.5],
//			}
      ]
    };

    //skills-graph
    //var getSkillsGraph = document.getElementById("skills-graph").getContext("2d");
    //var getTeamEffectivenessGraph = document.getElementById("team-effectiveness-graph").getContext("2d");


    //var teamEffectivenessData = {
    //  labels: teamEffectivenessQA['questions'],
    //  datasets: [
    //    {
    //      label: "You",
    //      backgroundColor: "#3a3b74",
    //      borderWidth: 0,
    //      hoverBackgroundColor: "#3a3b74",
    //      data: teamEffectivenessQA['answers']
    //    }
    //  ]
    //};

//    showGraphs.prototype.myTeamEffectivenessGraphs = function (){

      //var teamEffectivenessGraph = new Chart(getTeamEffectivenessGraph, {
      //  type: 'bar',
      //  data: teamEffectivenessData,
      //  options: {
      //    scales: {
      //      xAxes: [
      //        {
      //          display: false,
      //          barPercentage: 1,
      //          categoryPercentage: 0.7,
      //          gridLines: {
      //            display: false
      //          },
      //          ticks: {
      //            fontColor: "#8f9092"
      //          }
      //        }
      //      ],
      //      yAxes: [
      //        {
      //          display: true,
      //          scaleLabel: {
      //            show: true
      //          },
      //          gridLines: {
      //            color: "#ecedef"
      //          },
      //          ticks: {
      //            beginAtZero: true,
      //            stepSize: 1.3,
      //            fontColor: "#8f9092"
      //          }
      //        }
      //      ]
      //
      //    },
      //    legend: {
      //      position: 'bottom'
      //    }
      //  }
      //
      //});
//  };

//    showGraphs.prototype.myAllGraphs = function () {

      //skills-graph
      //var skillsGraph = new Chart(getSkillsGraph, {
      //  type: 'bar',
      //  data: data,
      //  options: {
      //    scales: {
      //      xAxes: [
      //        {
      //          display: false,
      //          barPercentage: 1,
      //          categoryPercentage: 0.7,
      //          gridLines: {
      //            display: false
      //          },
      //          ticks: {
      //            fontColor: "#8f9092"
      //          }
      //        }
      //      ],
      //      yAxes: [
      //        {
      //          display: true,
      //          scaleLabel: {
      //            show: true
      //          },
      //          gridLines: {
      //            color: "#ecedef"
      //          },
      //          ticks: {
      //            beginAtZero: true,
      //            stepSize: 1.3,
      //            fontColor: "#8f9092"
      //          }
      //        }
      //      ]
      //
      //    },
      //    legend: {
      //      position: 'bottom'
      //    }
      //  }
      //});

  //$("#skills-b").on("click", skillsGraph);
  $("#thriving-b").on("click", thrivingChart);
  //$("#team-effectiveness-b ").on("click", teamEffectivenessGraph);

//Data Format
//    [
//    {"name": "shows<br>consideration<br>for me", "size": 3000, "color": "#3c3467", "scaleFont":"35"},
//    {"name": "gives me<br>actionable<br>feedback", "size": 600, "color": "#d5d0ca", "scaleFont":"20"},
//    {"name": "keeps the<br>team focused", "size": 1800, "color": "#40a498", "scaleFont":"27"},
//    {"name": "informs<br>the team", "size": 2400, "color": "#486ca0", "scaleFont":"30"},
//    {"name": "shows<br>appreciation", "size": 3000, "color": "#3c3467", "scaleFont":"35"},
//    {"name": "has<br>positively<br>influenced<br>my growth", "size": 2400, "color": "#4a6aa5", "scaleFont":"30"},
//    {"name": "communicates<br>clear goals", "size": 1200, "color": "#88c7bc", "scaleFont":"23"}
//    ]



//console.log(dataPreparation());

