var companyInfographic = (function () {

  function companyInfographic () {
  }

  companyInfographic.prototype.peakIcon = function() {

    var imageWidth= 360,
        imageHeight= 267,
        ratio = imageHeight/imageWidth,
        leftMarker = 210,
        topMarker = 25,
        image_width_n = $("div.img-wrapper").width(),
        image_height_n = ratio * image_width_n,
        icon_location_l,
        icon_location_t;

    icon_location_l = image_width_n * leftMarker / imageWidth;
    icon_location_t = image_height_n * topMarker / imageHeight;

    $("div.peak-icon").css({
      'left': icon_location_l + 'px',
      'top': icon_location_t + 'px'
    });


  };

  companyInfographic.prototype.resizeBars = function () {
    var imgH = $(".img-wrapper img").height() - 4,
        itemH = imgH / 5;

    $(".bars-wrapper").css("height", imgH );
    $(".bars-wrapper ul li a").css({"height": itemH, "line-height" : itemH + "px" });

  };

  return companyInfographic

})();

$(document).ready(function() {
  var cInfographic = new companyInfographic ();
  cInfographic.peakIcon();
  setTimeout (cInfographic.resizeBars, 200);

  $(window).on("resize",function(){
    cInfographic.peakIcon();
    cInfographic.resizeBars()
  });

  $(window).bind('load',function () {
    cInfographic.peakIcon();
    cInfographic.resizeBars();
  });
});

/*show reports*/

var showSpecificPanel = function () {
	 
	 var targetPanel = $(this).text().split(" ").join('-');

	 $("#reports-wrapper").attr("aria-expanded","true"); 
	 $("#reports-wrapper").addClass("in");
	 
	 $("#thriving-panel, #team-effectiveness-panel, #manager-effectiveness-panel, #behaviors-panel, #skills-panel").hide();
	 $("#" + targetPanel + "-panel").show();

  $('.panel-title.text-capitalize.text-center').find('span:first-child').text(targetPanel.replace('-', ' '));

	$('html, body').animate({
		scrollTop: $("#reports-wrapper").offset().top
		}, 1000); 
		return false;	
};

$("#s-all-reports").on("click", function () {
	$("#thriving-panel, #team-effectiveness-panel, #manager-effectiveness-panel, #behaviors-panel, #skills-panel").show();
});
$("#thriving-b, #team-effectiveness-b, #manager-effectiveness-b, #behaviors-b, #skills-b").on("click", showSpecificPanel);

//$("#skills-b").on("click", function(e){
//  event.preventDefault();
//  $("#team-effectiveness-b").click();
//  $('.panel-title.text-capitalize.text-center').find('span:first-child').text('Skills');
//});

/*tooltip move and retype later*/
$(function () {
	$('[data-toggle="tooltip"]').tooltip({
		container: 'body'
		});
});
