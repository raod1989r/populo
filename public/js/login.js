function inputFocus() {
  $(this).prev("span").hide();
}

function inputBlur() {
  if (!$(this).val()) {
    $(this).prev("span").show();
  }
}

$("#username, #password").on("focus", inputFocus);
$("#username, #password").on("blur", inputBlur);

$(window).on('load', function() {
  setTimeout(function(){
    $('#login-form input[name=username]').val(null);
    $('#login-form input[name=password]').val(null);
  },1);
});
