/*tooltip move and retype later*/
$(function () {
	$('[data-toggle="tooltip"]').tooltip({
		container: 'body'
		});
});
	
	
$(document).ready(function() {

  	$(".c-select").select2();
	$("#select2-select-manager-container").html('<img src="img/manager-icon.svg"> Select Manager');
	$("#select2-select-individual-container").html('<img src="img/individual-icon.svg"> Select Individual');
	$("#select2-select-topic-container").html('<img src="img/topic-icon.svg"> Select Topic');
	
	$("span.select2.select2-container" ).on("click", function() {
		$(".select2-results" ).toggleClass("mCustomScrollbar").attr('data-mcs-theme', 'gray-theme');
            $(".select2-results").mCustomScrollbar("destroy");
			$(".select2-results").mCustomScrollbar({
		 	advanced:{
            	updateOnContentResize: true
            },
				scrollInertia:500
	 	});
	});
	
	
});

/*this is the default - once you know what's needed for graph/s retype the script*/

//var randomScalingFactor = function(){ return Math.round(Math.random()*6.5)};
//	var barChartData = {
//		labels : ["Skill #1","Skill #2","Skill #3","Skill #4","Skill #5"],
//		datasets : [
//			{
//				label: "You",
//				fillColor : "#005194",
//				strokeColor : "rgba(220,220,220,0.8)",
//				highlightFill: "rgba(220,220,220,0.75)",
//				highlightStroke: "rgba(220,220,220,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//			},
//			{
//				label: "Your Manager",
//				fillColor : "#256ba4",
//				strokeColor : "rgba(151,187,205,0.8)",
//				highlightFill : "rgba(151,187,205,0.75)",
//				highlightStroke : "rgba(151,187,205,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//			},
//			{
//				label: "Your Team",
//				fillColor : "#4d85b5",
//				strokeColor : "rgba(151,187,205,0.8)",
//				highlightFill : "rgba(151,187,205,0.75)",
//				highlightStroke : "rgba(151,187,205,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//			},
//			{
//				label: "Your Multi-Raters",
//				fillColor : "#739fc5",
//				strokeColor : "rgba(151,187,205,0.8)",
//				highlightFill : "rgba(151,187,205,0.75)",
//				highlightStroke : "rgba(151,187,205,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//			}
//		]
//	}
//
//		var ctx = document.getElementById("graph1").getContext("2d");
//		var myBar = new Chart(ctx).Bar(barChartData, {
//			responsive : true,
//			 scaleShowVerticalLines: false,
//			 scaleGridLineColor : "#ecedef",
//			 barShowStroke : false,
//			 barValueSpacing : 25,
//			 barDatasetSpacing : -1,
//			 tooltipFillColor: "#3c4858",
//			 tooltipCornerRadius: 2,
//			 scaleOverride: true,
//
//    scaleSteps: 5,
//    scaleStepWidth: 1.3,
//    scaleStartValue: 0,
//	legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>"
//
//		});
//
//	$('#graph1-legend').html(myBar.generateLegend());
