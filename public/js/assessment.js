var radioButtons = (function () {

  function radioButtons() {
  }

  radioButtons.prototype.radioClick = function () {

  var getValue = $(this).closest('li').index() + 1,
      lastClassTimeline = $(this).closest("ul").attr('class').split(' ').pop();
			
      $(this).closest("ul").find("li").each(function(i) {
        
        i++;
        if (i < getValue ){
            $(this).find("label").addClass("r-color-" + i);
        }else {
		    $(this).find("label").removeClass("r-color-" + i);
					 }
        });
			 
        if (lastClassTimeline !=="responses"){
		   $(this).closest("ul").removeClass(lastClassTimeline);
		}
        $(this).closest("ul").addClass("timeline" + getValue);

    $(this).closest("ul").find("input[type=radio]:checked + label.text-capitalize").addClass("r-checked-" + getValue + " " + "r-checked-b" + getValue + " " + "label-c-" + getValue);

    $(this).parents('.question').find('.selected-answer').attr('value', getValue - 1);
    $(this).parents('.question').find('.btn-not-able-tmj').removeClass('emotion-btn-active');
    $(this).parents('.question').find('.select-emotion-option').attr('value', "false");

    //highlight button
    $('.rocket-submit').find('img.replace-icon').attr('src', '/img/rocket-icon-blue.svg');
    $('.award-submit').find('img.replace-icon').attr('src', '/img/award-icon-blue.svg');
  };

  return radioButtons;

})();

var radioB = new radioButtons();

$('form ul.responses li input[type=radio]').on("click", radioB.radioClick);


/*tooltip move and retype later*/
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body'
  });

  $('.emotion-btn').click(function (event) {
    var current_ele = $(event.currentTarget);
    var inputEle = current_ele.prev();
    var inputValue = inputEle.val();

    if(!current_ele.hasClass('emotion-btn-active')){
      if (inputValue == "false") {

        $(this).parents('.question').find('ul.responses').attr('class', 'responses');
        $(this).parents('.question').find('ul.responses').find('label').attr('class', 'text-capitalize');
        $(this).parents('.question').find('ul.responses').find('input').prop('checked', false);
        inputEle.attr('value', "true");
      } else {

        $(this).parents('.question').find('ul.responses').attr('class', 'responses');
        $(this).parents('.question').find('ul.responses').find('label').attr('class', 'text-capitalize');
        $(this).parents('.question').find('ul.responses').find('input').prop('checked', false);
        inputEle.attr('value', "false");
      }

      current_ele.addClass('emotion-btn-active');
    };

    if(current_ele.parents('.question').hasClass('last-question')){
      //highlight button
      $('.rocket-submit').find('img.replace-icon').attr('src', '/img/rocket-icon-blue.svg');
      $('.award-submit').find('img.replace-icon').attr('src', '/img/award-icon-blue.svg');
    }
  });

  $('.submit-assessment-back-type').click(function (event) {
    $('form').append('<input class=hide value=back name=backBtn />');
    $('form').find('input[type="submit"]').click();
  });

  $('.radio-edit-select').click();

  /*stisky timeline*/
  function stickyTimeline() {
    var startPosition = $(".s-header").offset().top,
        topPosition = $(this).scrollTop(),
        winHeight = $(this).height(),
        docHeight = $(document).height();

    if (topPosition > startPosition && topPosition + winHeight < docHeight - 150) {
      $(".timeline-wrapper").addClass("sticky-timeline");
      $(".timeline-header, .timeline").addClass("sticky-timeline-inner center-block");
    } else {
      if (topPosition == 0 || topPosition + winHeight == docHeight) {
        $(".timeline-wrapper").removeClass("sticky-timeline");
        $(".timeline-header, .timeline").removeClass("sticky-timeline-inner center-block");
      }
    }

  }

  var lastQuestion = function () {
    if ($('.last-question .emotion-btn-active').length > 0) {
      //highlight button
      $('.rocket-submit').find('img.replace-icon').attr('src', '/img/rocket-icon-blue.svg');
      $('.award-submit').find('img.replace-icon').attr('src', '/img/award-icon-blue.svg');
    }
  };

  lastQuestion();

  $(window).on("scroll touchmove", stickyTimeline);


  /*insert textarea*/
  var addTextArea = function () {
    $(this).next(".txt-area-wrapper").toggle("show");
    var src = ($(this).find("img").attr("src") === 'images/add-icon-white.svg')
        ? 'images/remove-icon-white.svg'
        : 'images/add-icon-white.svg';
    $(this).find("img").attr('src', src);
  }

  $("#i-c-1").on("click", addTextArea);
  $("#i-c-2").on("click", addTextArea);

  var checkedRadioButtons = function () {
    var all_selected = true;
    $("input:radio").each(function(){
      var name = $(this).attr("name"),
          emotionBtn = $(this).closest(".col-xs-12").find("button");

      if(!$("input[type=radio][name="+name+"]").is(':checked') && !emotionBtn.hasClass("emotion-btn-active") ){
        all_selected = false;
      }
    });

    if ( all_selected == false ) {
      $(".error-msg").addClass("show");
      $('html, body').animate({
        scrollTop: $(".error-msg").offset().top - 90
      }, 1000);
      return false;
    }else {
      return true;
    }
  }

  function nextRadioButtonsCheck(event){
    event.preventDefault();
    if(checkedRadioButtons()){
      $('form').find('input[type="submit"]').click();
    }
  }

  function checkQuestionsOnSubmit(){
    event.preventDefault();
    if(checkedRadioButtons()){
      $('form').append('<input class=hide value=submit name=submitBtn />');
      $('form').find('input[type="submit"]').click();
    }
  }

  function checkQuestionsOnSave(){
    event.preventDefault();
    if(checkedRadioButtons()){
      $('form').append('<input class=hide value=save name=saveBtn />');
      $('form').find('input[type="submit"]').click();
    }
  }

  $(".submit-assessment-btn").on("click", checkQuestionsOnSubmit);
  $(".save-assessment-btn").on("click", checkQuestionsOnSave);
  $("#nextQ").on("click", nextRadioButtonsCheck);
});
