$('#select-manager').change(function () {
  var selectedManagerId = $('#select-manager option:selected').val();
  var managersEmployees = $('#select-manager').data('options');
  var selectedEmployees = [];
  var data = [];

  managersEmployees.forEach(function (managerEmployees) {
    if (selectedManagerId == managerEmployees['_id']) {
      selectedEmployees = managerEmployees['employees'].sort();
    }
  });

  selectedEmployees.forEach(function (employee) {
    data.push({id: employee, text: employee});
  });

  $("#select-individual").html('');

  $("#select-individual").select2({
    data: data
  })
});

$('#get-insights').click(function () {
  var manager = $('#select-manager').val();
  var individual = $('#select-individual').val();
  var assessmentType = $('#select-topic').val().toUpperCase();

  if (individual != null) {
    $.ajax({
      url: '/getInsights',
      data: {manager: manager, individual: individual, assessmentType: assessmentType},
      method: 'POST'
    }).done(function (data) {
          $('#thriving-panel, #team-effectiveness-panel, #manager-effectiveness-panel, #behaviors-panel, #skills-panel').addClass('hide');

          if (data.isEmpty) {
            $('#thriving-panel, #team-effectiveness-panel, #manager-effectiveness-panel, #behaviors-panel, #skills-panel').addClass('hide');
          }else{
            switch (assessmentType) {
              case 'SKILLS':
                skillsBarData(data.submissionQA);
                ChartRenderer._renderSkillVBarChart();
                $('#skills-panel').removeClass('hide');
                break;
              case 'THRIVING':
                var datasets = [];
                var questions = data.submissionQA['questions'];

                data.submissionQA['answers'].forEach(function (answer, index) {

                  var data = {
                    data: [answer, 5 - answer],
                    backgroundColor: [
                      doughnutColors[answer],
                      "#ecedf0"
                    ],
                    hoverBorderColor: [
                      "#fff",
                      "#fff"
                    ],

                    labels: questions

                  };
                  datasets.push(data);
                });

                thrivingChart['data']['datasets'] = datasets;
                thrivingChart.options.tooltips = {
                  callbacks: {
                    label: function (tooltipItem, data) {
                      return questions[tooltipItem.datasetIndex];
                    }
                  }
                };

                thrivingChart.update();
                $('#thriving-panel').removeClass('hide');
                break;
              case 'TEAM EFFECTIVENESS':
                teamEffectivenessBarData(data.submissionQA);
                ChartRenderer._renderTeamEffectivenessVBarChart();
                $('#team-effectiveness-panel').removeClass('hide');
                break;
              case 'MANAGER EFFECTIVENESS':
                var chart1Data = chart1DataPreparation(data.submissionQA);
                var chart2Data = chart2DataPreparation(data.submissionQA);

                ChartRenderer._renderBubbleHBarChart(chart1Data, chart2Data);

                $('#manager-effectiveness-panel').removeClass('hide');
                break;
              case 'BEHAVIORS':
                $('#behaviors-panel').removeClass('hide');
                break;
              default:
                console.log('out of option');
            }
          }
        }
    ).
        fail(function (data) {
          console.log(data);
        })
  }
});
