var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    Survey = mongoose.model('Survey'),
    Submission = mongoose.model('Submission'),
    _ = require('underscore'),
    helper = require('../../config/helper.js');

module.exports = function (app) {
  app.post('/getInsights',
      helper.isLoggedIn,
      function (req, res) {
        var individualId = req.body.individual;
        var assessmentType = req.body.assessmentType;

        User.findOne({full_legal_name: individualId}, function (err, user) {
          if (user) {
            Survey.findOne({state: 'started'}, function (err, survey) {
              if (survey) {
                Submission.findOne({survey: survey._id, tag: assessmentType, user: user._id},
                    function (err, submission) {
                      if (submission) {
                        var submissionQA = submission.getQuestionsAndAnswers();
                      } else {
                        var isEmpty = true;
                        var submissionQA = {answers: [], questions: []};
                      }
                      res.json({status: 'success', submissionQA: submissionQA, isEmpty: isEmpty});
                    })
              } else {
                res.json({status: 'failure'});
              }
            });
          } else {
            res.json({status: 'user not found: ' + individualId});
          }
        })
      });
};
