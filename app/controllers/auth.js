var express = require('express'),
    router = express.Router(),
    _ = require('underscore'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    pdf = require('html-pdf'),
    helper = require('../../config/helper.js'),
    User = mongoose.model('User'),
    Notification = mongoose.model('Notification'),
    Assessment = mongoose.model('Assessment'),
    config = require('../../config/config.js'),
    Submission = mongoose.model('Submission'),
    Survey = mongoose.model('Survey');
    UserBehavior = mongoose.model('UserBehavior');

module.exports = function (app) {

  app.get('/',
      function (req, res) {
        if (req.isAuthenticated()) {
          res.redirect('dashboard')
        } else {
          res.render('login', {title: 'message'});
        }
      });

  app.post('/login', passport.authenticate('login', {
    successRedirect: '/dashboard',
    failureRedirect: '/',
    failureFlash: true
  }));

  app.get('/dashboard', helper.isLoggedIn,
      function (req, res) {
		
        Submission.find({user: req.user._id}).populate('assessment').then(function (submissions) {
          var skillAssessmentQA = '';
          var thrivingQA = '';
          var managerEffectivenessQA = '';
          var teamEffectivenessQA = '';
          var isSkillEmpty = false;
          var isThrivingEmpty = false;
          var isManagerEffectivenessEmpty = false;
          var isTeamEffectivenessEmpty = false;
          var behaviorData = [];

          _.each(submissions, function (submission) {
            if (submission.tag == 'SKILLS') {
              skillAssessmentQA = submission.getQuestionsAndAnswers();
            } else if (submission.tag == 'THRIVING') {
              thrivingQA = submission.getQuestionsAndAnswers();
            } else if(submission.tag == 'MANAGER EFFECTIVENESS'){
              managerEffectivenessQA = submission.getQuestionsAndAnswers();
            } else if(submission.tag == 'TEAM EFFECTIVENESS'){
              teamEffectivenessQA = submission.getQuestionsAndAnswers();
            }
          });

          Notification.find({notifierId: req.user._id, read: false}, function (err, notifications) {
            Survey.findOne({"raters.email": req.user.email}).then(function (survey) {
			  UserBehavior.find({emp_id: req.user.employee_id},{"created_at":0,"updated_at":0,"_id":0,"assessment":0,"__v":0}).then(function (behaviors) {
              _.each(survey.assessments, function (assessment) {
                var questions = _.pluck(assessment.questions, 'title');
                var answers = new Array(questions.length).fill(0);

                if (skillAssessmentQA == '' && assessment.tag == 'SKILLS') {
                  isSkillEmpty = true;
                  skillAssessmentQA = {questions: questions, answers: answers}
                } else if (thrivingQA == '' && assessment.tag == 'THRIVING') {
                  isThrivingEmpty = true;
                  thrivingQA = {questions: questions, answers: answers}
                } else if(managerEffectivenessQA == '' && assessment.tag == 'MANAGER EFFECTIVENESS'){
                  isManagerEffectivenessEmpty = true;
                  managerEffectivenessQA = {questions: questions, answers: answers}
                } else if(teamEffectivenessQA == '' && assessment.tag == 'TEAM EFFECTIVENESS'){
                  isTeamEffectivenessEmpty = true;
                  teamEffectivenessQA = {questions: questions, answers: answers}
                }
              });

              var submittedNotification = _.find(notifications, function(notification){
                return (notification.type.notificationType == 'SurveySubmission')
              });
//

			
			  // UserBehavior.find({emp_id: "001412"}).then(function (behavior) {
					// if(behavior) {
						// behaviorData = behavior;
					// }
				// });
				
				// console.log(behaviorData);
				//console.log(behaviors);
			
		      var response = {
                message: req.flash('successfully logged in'), user: req.user, isSkillEmpty: isSkillEmpty,
                isThrivingEmpty: isThrivingEmpty, thrivingQA: JSON.stringify(thrivingQA),
                skillsQA: JSON.stringify(skillAssessmentQA), notifications: notifications,
                isManagerEffectivenessEmpty: isManagerEffectivenessEmpty,
                managerEffectivenessQA: JSON.stringify(managerEffectivenessQA),
                isTeamEffectivenessEmpty: isTeamEffectivenessEmpty,
                teamEffectivenessQA: JSON.stringify(teamEffectivenessQA),
                userName: req.user.full_legal_name, survey: survey, submittedNotification: submittedNotification, behavior: JSON.stringify(behaviors)
              };

              res.render('employee/dashboard', response);
            });
            })
          })
        });
      });

  app.get('/manager-insights', helper.isLoggedIn,
      function (req, res) {
        Notification.find({notifierId: req.user._id, read: false}, function (err, notifications) {
          Survey.find({}, function (err, surveys) {
            User.findOne({employee_id: req.user.employee_id},
                function (err, manager) {

                    User.find({ancestors: manager.employee_id}, function (err, employees) {
                      employees = _.sortBy(employees, 'full_legal_name');

                      var response = {
                        message: req.flash('successfully loggedin'), user: req.user, employees: employees,
                        notifications: notifications, userName: req.user.full_legal_name, surveys: surveys,
                        assessmentTags: constants.human_tag_names
                      };

                      if (req.user.role == 'Manager') {
                        res.render('manager/dashboard', response);
                      } else {
                        res.redirect('/');
                      }
                    });
                });
          })
        })
      });

  app.get('/hr-insights', helper.isLoggedIn,
      function (req, res) {
        Notification.find({notifierId: req.user._id, read: false}, function (err, notifications) {
          Survey.find({}, function (err, surveys) {
            User.aggregate([{$match: {role: 'Manager'}}, {
              $group: {
                _id: "$manager_full_legal_name",
                employees: {"$addToSet": "$full_legal_name"}
              }
            }], function(err, managerEmployees){
              var managers = _.sortBy(managerEmployees, '_id');
//
//              var managerEmployees = _.map(managers, function (manager) {
//                var employees = _.filter(users, function (user) {
//                  return (manager.employee_id == user.manager_id);
//                });
//
//                employees = _.sortBy(employees, 'full_legal_name');
//                return {manager: manager, employees: employees}
//              });

              var response = {
                message: req.flash('successfully loggedin'), user: req.user, managerEmployees: managers,
                notifications: notifications, userName: req.user.full_legal_name, surveys: surveys,
                assessmentTags: constants.human_tag_names
              };

              if (req.user.role == 'HR Manager') {
                res.render('hr-manager/dashboard', response);
              } else {
                res.redirect('/');
              }
            })
          })
        })
      });

  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
  });
};


