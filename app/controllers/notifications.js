var express = require('express'),
    router = express.Router(),
    _ = require('underscore'),
    jade = require('jade'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    Survey = mongoose.model('Survey'),
    helper = require('../../config/helper.js'),
    Assessment = mongoose.model('Assessment'),
    Notification = mongoose.model('Notification');

module.exports = function (app) {
  app.get('/notifications/:notificationId',
      helper.isLoggedIn,
      function (req, res) {
        var notificationId = req.params.notificationId;

        Notification.findOne({_id: notificationId}, function (err, notification) {
          if (err) {
            res.render('error');
          } else {
            var redirect_url = notification.redirectSourceURL();
            res.redirect(redirect_url);
          }
        })
      });

  app.get('/submission',
      helper.isLoggedIn,
      function (req, res) {
        var user = req.user;

        user.generateReport();
        Notification.readSubmissionNotification(user);

        res.render('employee/submission', {
          user: req.user
        });
      });

  app.get('/send/reminders/:surveyId', function (req, res) {
    var surveyId = req.params.surveyId;

    Survey.findOne(surveyId, function(err, survey){
      _.each(survey.assignees, function(user){

        var html = jade.renderFile('./app/views/hr-manager/email_reminder.jade', {user: user});

        var mailOptions = {
          from: 'dev.populo@gmail.com',
          to: 'dev.populo@gmail.com',
          subject: 'Submit Assessment',
          html: html
        };

        app.smtpTransport.sendMail(mailOptions, function(error, info){
          if(error){
            return console.log(error);
          }
          console.log('Message sent: ' + info.response);
        });

//        Submitting Reminder Notifications
//        survey.surveyReminder(req.user, user);
      });
      if(req.user.role == 'HR Manager'){
        res.redirect('/hr-insights')
      }else if(req.user.role == 'Manager'){
        res.redirect('/manager-insights')
      }else{
        res.redirect('/dashboard')
      }
    });
  });
};
