var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    UserBehavior = mongoose.model('UserBehavior'),
    _ = require('underscore'),
    winston = require('winston'),
    xlsx = require('xlsx'),
    formidable = require('formidable'),
    fs = require('fs'),
    helper = require('../../config/helper.js');

module.exports = function (app) {
  app.post('/xlsx/users',
      helper.isAPIAuthenticated,
      function (req, res) {

        var fileName = 'userExcel';
        var form = new formidable.IncomingForm();

        form.parse(req, function (err, fields, files) {
          var filePath = files[fileName].path;
          var usersJSON = helper.excelImportJSON(xlsx, filePath);

          User.importFromDocs(usersJSON, function () {
            res.json({status: 'successfully processed'});
          });
        });
      });

  app.post('/xlsx/behaviors',
      helper.isAPIAuthenticated,
      function (req, res) {

        var fileName = 'behaviorsExcel';
        var form = new formidable.IncomingForm();

        form.parse(req, function (err, fields, files) {
          var filePath = files[fileName].path;
          var behaviorsJSON = helper.excelImportJSON(xlsx, filePath);

          UserBehavior.importFromDocs(behaviorsJSON, function () {
            res.json({status: 'successfully processed'});
          });
        });
      });

  app.get('/filespath',
      helper.isAPIAuthenticated,
      function (req, res) {
        var array = [];

        fs.readdir('public/pdf', function (err, files) {
          if (err) return;
          files.forEach(function (f) {
            array.push(req.hostname + '/files/pdf/' + f);
          });

          res.json(array);
        });
      }
  );

  app.get('/generate-report/:userId',
//      helper.isLoggedIn,
      function (req, res) {
        var userId = req.params.userId;
        var Submission = mongoose.model('Submission');
        var User = mongoose.model('User');

        var skillsQA = '';
        var thrivingQA = '';
        var managerEffectivenessQA = '';
        var teamEffectivenessQA = '';
        var behaviorsQA = '';


        User.findOne({_id: userId}, function (err, user) {
          console.log(userId);
          Submission.find({user: user._id}, function (err, submissions) {
            if (submissions) {
              var thriving = _.find(submissions, function (submission) {
                thrivingQA = submission.getQuestionsAndAnswers();
                return (submission.tag == 'THRIVING')
              });
              var team_effectiveness = _.find(submissions, function (submission) {
                teamEffectivenessQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'TEAM EFFECTIVENESS'
              });
              var manager_effectiveness = _.find(submissions, function (submission) {
                managerEffectivenessQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'MANAGER EFFECTIVENESS'
              });
              var behaviors = _.find(submissions, function (submission) {
                behaviorsQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'BEHAVIORS'
              });
              var skills = _.find(submissions, function (submission) {
                skillsQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'SKILLS'
              });

              res.render('pdf-templates/reports', {
                user: user, submission: submissions, thriving: thriving,
                team_effectiveness: team_effectiveness, manager_effectiveness: manager_effectiveness,
                behaviors: behaviors, skills: skills, thrivingQA: JSON.stringify(thrivingQA), teamEffectivenessQA: JSON.stringify(teamEffectivenessQA),
                managerEffectivenessQA: JSON.stringify(managerEffectivenessQA), behaviorsQA: JSON.stringify(behaviorsQA), skillsQA: JSON.stringify(skillsQA)
              });
            }
          });
        })
      });

  app.get('/sample-report',
      helper.isLoggedIn,
      function (req, res) {
        var _this = req.user;
        var Submission = mongoose.model('Submission');
        var User = mongoose.model('User');

        var skillsQA = '';
        var thrivingQA = '';
        var managerEffectivenessQA = '';
        var teamEffectivenessQA = '';
        var behaviorsQA = '';


        User.findOne({}, function (err, user) {
          Submission.find({user: _this._id}, function (err, submissions) {
            if (submissions) {
              var thriving = _.find(submissions, function (submission) {
                thrivingQA = submission.getQuestionsAndAnswers();
                return (submission.tag == 'THRIVING')
              });
              var team_effectiveness = _.find(submissions, function (submission) {
                teamEffectivenessQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'TEAM EFFECTIVENESS'
              });
              var manager_effectiveness = _.find(submissions, function (submission) {
                managerEffectivenessQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'MANAGER EFFECTIVENESS'
              });
              var behaviors = _.find(submissions, function (submission) {
                behaviorsQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'BEHAVIORS'
              });
              var skills = _.find(submissions, function (submission) {
                skillsQA = submission.getQuestionsAndAnswers();
                return submission.tag == 'SKILLS'
              });

              res.render('pdf-templates/reports', {
                user: user, submission: submissions, thriving: thriving,
                team_effectiveness: team_effectiveness, manager_effectiveness: manager_effectiveness,
                behaviors: behaviors, skills: skills, thrivingQA: JSON.stringify(thrivingQA), teamEffectivenessQA: JSON.stringify(teamEffectivenessQA),
                managerEffectivenessQA: JSON.stringify(managerEffectivenessQA), behaviorsQA: JSON.stringify(behaviorsQA), skillsQA: JSON.stringify(skillsQA)
              });
            }
          });
        })
      });

};
