var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    _ = require('underscore'),
    winston = require('winston'),
    helper = require('../../config/helper.js'),
    Assessment = mongoose.model('Assessment'),
    Survey = mongoose.model('Survey'),
    Submission = mongoose.model('Submission'),
    Question = mongoose.model('Question'),
    Comment = mongoose.model('Comment'),
    Answer = mongoose.model('Answer');

module.exports = function (app) {
  app.get('/assessments/:surveyId',
      helper.isLoggedIn,
      function (req, res) {
        var surveyId = req.params.surveyId;
        var step = req.params.step || 0;

        Survey.findById(surveyId, function (err, survey) {
          if (err) {
            res.render('error');
          } else {
            var allassessments = survey.assessments;
            var assessments = _.reject(allassessments, function (assessment) {
              return assessment.tag == 'BEHAVIORS';
            });

            var questionsCount = _.flatten(_.map(assessments, function (assessment) {
              return assessment.questions;
            })).length;

            var completedQuestions = _.flatten(_.map(assessments.slice(0, step + 1), function (assessment) {
              return assessment.questions;
            })).length;

            var statusPercentage = (completedQuestions / questionsCount) * 100;
            var assessment = assessments[step];

            Submission.findOne({
              survey: survey,
              assessment: assessment,
              user: req.user
            }, function (err, submission) {
              if (submission) {
                var template = 'employee/edit_assessment';
              } else {
                var template = 'employee/assessments';
              }

              var buttonsText = Assessment.buttonText(assessment);

              res.render(template, {
                user: req.user,
                assessment: assessment, completedQuestions: completedQuestions,
                questionsCount: questionsCount, survey: survey, step: step,
                statusPercentage: statusPercentage, submission: submission,
                buttonsText: buttonsText
              })
            });
          }
        })
      });

  app.post('/assessments/:surveyId',
      helper.isLoggedIn,
      function (req, res) {
        var surveyId = req.params.surveyId;
        var step = parseInt(req.body.step) || 0;
        var prevStep = parseInt(req.body.prevStep) || 0;

        if (req.body.backBtn == 'back') {
          step = step - 2;
        } else if (req.body.submitBtn == 'submit') {
          step = step - 1;
        } else if (req.body.saveBtn == 'save') {
          step = step - 1;
        }

        Survey.findById(surveyId, function (err, survey) {
          if (err) {
            res.render('error');
          } else {

            var allassessments = survey.assessments;
            var assessments = _.reject(allassessments, function (assessment) {
              return assessment.tag == 'BEHAVIORS';
            });

            if (assessments.length > step) {
              var questionsCount = _.flatten(_.map(assessments, function (assessment) {
                return assessment.questions;
              })).length;

              var completedQuestions = _.flatten(_.map(assessments.slice(0, step + 1), function (assessment) {
                return assessment.questions;
              })).length;

              var statusPercentage = (completedQuestions / questionsCount) * 100;
              var assessment = assessments[step];

              if (req.body.backBtn == 'back') {
                var submitAssessment = assessments[step + 1];
              } else if (req.body.submitBtn == 'submit') {
                var submitAssessment = assessments[step];
                var assessment = assessments[step];
              } else if (req.body.saveBtn == 'save') {
                var submitAssessment = assessments[step];
                var assessment = assessments[step];
              } else {
                var submitAssessment = assessments[step - 1];
              }

              Submission.findOne({
                    survey: survey,
                    assessment: submitAssessment,
                    user: req.user
                  }, function (err, submission) {

                    if (submission) {
                      console.log('Update existing');
                      submission.answers = [];
                      submission.comments = [];
                    } else {
                      console.log('Creating submission record');
                      var submission = new Submission();
                      submission.survey = survey;
                      submission.assessment = submitAssessment;
                      submission.tag = submitAssessment.tag;
                      submission.user = req.user;
                    }

                    var questionIds = _.map(req.body.questions, function (answerOption, questionId) {
                      return questionId
                    });

                    var commentIds = _.map(req.body.comments, function (commentAnswer, commentId) {
                      return commentId;
                    });

                    Question.find({'_id': {$in: questionIds}}, function (err, questions) {
                      var answers = _.map(questions, function (question) {
                        var answer = new Answer();
                        var questionId = question._id;
                        answer.user = req.user;
                        answer.question = question;

                        if (req.body.question_emotions[questionId] == 'true') {
                          answer.selectedOption = '';
                        } else {
                          answer.selectedOption = req.body.questions[questionId];
                        }

                        answer.selectedEmotion = req.body.question_emotions[questionId];
                        answer.save();
                        return answer;
                      });

                      Question.find({'_id': {$in: commentIds}}, function (err, commentedQuestions) {
                        var comments = _.map(commentedQuestions, function (commentQuestion) {
                          var comment = new Comment();
                          var commentId = commentQuestion._id;

                          comment.question = commentId;
                          comment.answerText = req.body.comments[commentId];
                          comment.save();
                          return comment;
                        });

                        submission.answers = answers;
                        submission.comments = comments;
                        submission.save(function () {

                          Submission.findOne({
                            survey: survey,
                            assessment: assessment,
                            user: req.user
                          }, function (err, submissionQuestions) {

                            if (submissionQuestions) {
                              var template = 'employee/edit_assessment';
                            } else {
                              var template = 'employee/assessments';
                            }

                            if (assessment.tag == 'SKILLS') {
                              if (submissionQuestions) {
                                var template = 'employee/edit_skill_assessment';
                              } else {
                                var template = 'employee/skill_assessments';
                              }
                            }

                            if (req.body.submitBtn == 'submit') {
                              survey.submissionNotificationRead(req.user._id);
                              res.redirect('/submission');
                            }

                            var buttonsText = Assessment.buttonText(assessment);

                            res.render(template, {
                              user: req.user,
                              assessment: assessment, completedQuestions: completedQuestions,
                              questionsCount: questionsCount, survey: survey, step: step,
                              statusPercentage: statusPercentage, prevStep: prevStep,
                              submission: submissionQuestions, buttonsText: buttonsText
                            });
                          });
                        });
                      });
                    });
                  }
              )
            }
            else {
              survey.submissionNotificationRead(req.user._id);
              res.redirect('/submission');
            }
          }
        })
      }
  )
};
