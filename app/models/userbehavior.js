'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    ObjectId = Schema.ObjectId;

var fields = {
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  assessment: {
    type: Schema.Types.ObjectId,
    ref: 'Assessment'
  },
  emp_id: {
    type: String
  },
  evaluation_type: {
    type: String
  },
  evaluation_status: {
    type: String
  },
  evaluator_id: {
    type: String
  },
  evaluator_name: {
    type: String
  },
  question_id: {
    type: String
  },
  question: {
    type: String
  },
  rating_id: {
    type: String
  },
  rating: {
    type: String
  },
  comment: {
    type: String
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
};

var userBehaviorSchema = new Schema(fields);

userBehaviorSchema.plugin(require('mongoose-lifecycle'));

userBehaviorSchema.statics.importFromDocs = function importFromDocs(behaviorsJSON, callback) {
  var _this = this;
  var UserBehavior = mongoose.model('UserBehavior');

  _this.remove({}, function (err, docs) {
    if (err) {
      console.log('unable to clear user behaviors');
    } else {
      console.log('clear user behaviors');
    }
  });

  var behaviors = _.map(behaviorsJSON, function (behavior) {
	
	console.log("=====================================");
	
	var comptencyArray = [];
	
	comptencyArray[""]="0";
	comptencyArray["COMPETENCY_LEVEL-9-8"]="0";
	comptencyArray["COMPETENCY_LEVEL-9-9"]="1";
	comptencyArray["COMPETENCY_LEVEL-9-10"]="2";
	comptencyArray["COMPETENCY_LEVEL-9-11"]="3";
	comptencyArray["COMPETENCY_LEVEL-9-12"]="4";
	comptencyArray["COMPETENCY_LEVEL-9-13"]="5";
	
    var newBehavior = new UserBehavior();
    newBehavior.emp_id = behavior['Employee ID'];
    newBehavior.evaluation_type = behavior['Evaluation Type'];
    newBehavior.evaluation_status = behavior['Evaluation Complete?'];
    newBehavior.evaluator_id = behavior['Evaluator ID'];
    newBehavior.evaluator_name = behavior['Evaluator'];
    newBehavior.question_id = behavior['Question ID'];
    newBehavior.question = behavior['Question'];
    newBehavior.rating_id = comptencyArray[behavior['Rating ID']];
    newBehavior.rating = behavior['Rating'];
    newBehavior.comment = behavior['Comment'];
	console.log(newBehavior);
    newBehavior.save(function(err, doc){
      //console.log(err);
      if(doc){
        doc.updateAssessment();
        //console.log('Added behavior');
      }
    });
  });
  callback();
};

userBehaviorSchema.methods.updateAssessment = function updateAssessment() {
  var _this = this;
  var Survey = mongoose.model('Survey');

  Survey.findOne({state: 'started'}, function (err, survey) {
    if (survey) {
      var assessments = _.filter(survey.assessments, function (assessment) {
        return (assessment.tag == 'BEHAVIORS');
      });

      var behaviorAssessment = assessments[0];
      _this.assessment = behaviorAssessment;
//      behavior.user = behaviorAssessment.populate('user').user._id;
      _this.save(function (err, doc) {
        //console.log(err);
        if (doc) {
          //console.log('behavior updated');
        }
      });
    }
  });
};

module.exports = mongoose.model('UserBehavior', userBehaviorSchema);
