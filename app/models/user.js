'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    helper = require('../../config/helper.js'),
    fs = require('fs'),
    pdf = require('html-pdf'),
    jade = require('jade'),
    _ = require('underscore');

var fields = {
//  email: {
//    type: String
//  },
  encrypted_password: {
    type: String
  },
  superior_organization: {
    type: String
  },
  supervisory_organization: {
    type: String
  },
  manager_location: {
    type: String
  },
  manager_id: {
    type: String
  },
  manager_full_legal_name: {
    type: String
  },
  job_family_group: {
    type: String
  },
  job_family: {
    type: String
  },
  job_profile: {
    type: String
  },
  employee_id: {
    type: String
  },
  full_legal_name: {
    type: String
  },
  ancestors: [],
  type: {
    type: String
  },
  location: {
    type: String
  },
  role: {
    type: String
  },
  avatar: {
    type: String,
    default: '/img/avatar-f-big.svg'
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
};

var userSchema = new Schema(fields);

userSchema.statics.registerUser = function registerUser(user, role) {
  var password = 'testtest1';

  user.last_name = '';
  user.role = role;
//  user.email = userEmail;
//  user.full_legal_name = 'User';
  user.encrypted_password = helper.encryptPassword(password);

  return user.save(function (err) {
    if (err) {
      console.log('Error in Saving user: ' + err);
      throw err;
    }
    console.log('User Registration successfully ' + user.full_legal_name);
  });
};

userSchema.statics.importFromDocs = function importFromDocs(usersJSON, callback) {
  var _this = this;

  var filteredChicagoUsers = _.filter(usersJSON, function (user) {
    return (user['Manager Location'] == 'IMC FM Chicago')
  });

  var users = _.map(filteredChicagoUsers, function (user) {
    var password = 'testtest1';
//    var generatedEmail = user['Employee ID'] + '@populo.com';
    var encryptedPassword = helper.encryptPassword(password);
    var userType = user['User Type'];
    var userRole = 'Manager';
    //var userRole = (userType == 'Team Member' ? 'Employee' :
    //    (userType == 'Leader') ? 'Manager' : 'HR Manager');
    var ancestors = helper.getAncestors(usersJSON, user);

    return {
//      email: generatedEmail,
      role: userRole,
      ancestors: ancestors,
      type: user['User Type'],
      location: user['Location'],
      job_family: user['Job Family'],
      manager_id: user['Manager ID'],
      employee_id: user['Employee ID'],
      job_profile: user['Job Profile'],
      encrypted_password: encryptedPassword,
      full_legal_name: user['Full Legal Name'],
      manager_location: user['Manager Location'],
      job_family_group: user['Job Family Group'],
      superior_organization: user['Superior Organization'],
      supervisory_organization: user['Supervisory Organization'],
      manager_full_legal_name: user['Manager - Full Legal Name']
    };
  });

  _this.remove({type: {$ne: null}}, function () {
    _this.collection.insert(users, function (err, docs) {
      callback();
    });
  })
};

userSchema.methods.assignedEmployees = function assignedEmployees() {
  var manager = this;
  var User = mongoose.model('User');

  var promise = User.find({employee_id: {$in: manager.ancestors}}).then(function (employees) {
    return employees;
  });

  return promise;
};

userSchema.methods.generateReport = function generateReport() {
  var _this = this;
  var fs = require('fs');
  var conversion = require("phantom-html-to-pdf")();

  var address = "http://localhost:3000/generate-report/" + _this._id;
  var output = './public/pdf/' + _this.full_legal_name + ' Growth Report.pdf'
  var fileNew = fs.createWriteStream(output);

  conversion({ url: address,
    viewportSize: {
      width: 970,
      height: 1050
    },
    zoomFactor: 1.30,
    paperSize: {
      format: 'A4',
      orientation: 'portrait',
      border: '1cm'
    },
    format: {
      quality: 100
    },
    printDelay: 1000,
    settings: {
      javascriptEnabled: true
    }
  }, function (err, pdf) {
    console.log(pdf.logs);
    pdf.stream.pipe(fileNew);
  });
};

userSchema.methods.latestSkillSubmissions = function latestSkillSubmissions() {
  var _this = this;
  var Submission = mongoose.model('Submission');

  Submission.findOne({user: _this._id, tag: 'SKILLS'}).sort({created_at: -1}).exec(function (err, submission) {
    var skillAssessmentQA = '';

    if (submission) {
      skillAssessmentQA = submission.questionAnswersAggr;
    } else {
      var questions = [0];
      var answers = [0];

      skillAssessmentQA = {questions: questions, answers: answers}
    }

    return skillAssessmentQA;
  });
};

module.exports = mongoose.model('User', userSchema);
