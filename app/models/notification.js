'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var fields = {
  text: {
    type: String
  },
  url: {
    type: String
  },
  read: {
    type: Boolean,
    default: false
  },
  notifierId: {
    type: String
  },
  type: {}
};

var notificationSchema = new Schema(fields);

notificationSchema.statics.readSubmissionNotification = function readSubmissionNotification(user) {
  var Survey = mongoose.model('Survey');
  var Notification = mongoose.model('Notification');

  Notification.find({notifierId: user._id, 'type.notificationType': 'SurveySubmission'}, function(err, notifications){
    notifications.forEach(function(notification){
      notification.readNotification();

      notification.save(function(doc){
        console.log(doc);
      });
    })
  });

  Survey.findOne({"raters._id": user._id}).then(function (survey) {
    Notification.findOne({notifierId: user._id, 'type.notificationType': 'Survey'}, function (err, notification) {
      notification.readNotification();

      var notification = new Notification();

      notification.text = 'Thank you for completing your self-assessment!';
      notification.url = '/notifications/' + notification._id;
      notification.notifierId = user._id;
      notification.read = false;
      notification.type = {
        initiatorId: user._id,
        notifierId: user._id,
        typeId: survey._id,
        notificationType: 'SurveySubmission'
      };

      notification.save(function (err, notification) {
        console.log('Notification Placed successfully -> ' + notification._id);
      });
    });
  });
}

notificationSchema.methods.redirectSourceURL = function redirectSourceURL() {
  var url = '';
  //this.read = true;
  //this.save();

  if (this.type.notificationType == 'Survey') {
    url = '/assessments/' + this.type.typeId;
  } else {
    url = '/assessments/' + this.type.typeId;
//    this.readNotification();
  }
  return url;
};

notificationSchema.methods.readNotification = function readNotification(){
  this.read = true;
  this.save(function (err, doc) {
    if (err) {
      console.log(err);
    } else {
      console.log('Successfully mark as read');
    }
  });
};

module.exports = mongoose.model('Notification', notificationSchema);
