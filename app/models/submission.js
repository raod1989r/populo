'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    winston = require('winston'),
    _ = require('underscore'),
    Question = mongoose.model('Question'),
    Answer = mongoose.model('Answer'),
    config = require('../../config/config.js');

//Loading constants
require(config.root + '/config/constants');


var fields = {
  survey: {
    type: Schema.Types.ObjectId,
    ref: 'Survey'
  },
  answers: [],
  comments: [],
  assessment: {
    type: Schema.Types.ObjectId,
    ref: 'Assessment'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  state: {
    type: String,
    enum: ['save', 'submit']
  },
  optionsSaved: {
    type: Object
  },
  questionAnswersAggr: {
    type: Object
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  tag: {
    type: String
  }
};

var submissionSchema = new Schema(fields);

submissionSchema.plugin(require('mongoose-lifecycle'));

submissionSchema.methods.getProgress = function getProgress() {
  var tags = constants['tag_names'];
  var progress = ((_.findIndex(tags, this.populate('assessment').tag) + 1) / tags.length) * 100;

  return progress;
};

submissionSchema.methods.processAnswers = function processAnswers() {
  this.optionsSaved = _.map(this.answers, function (answer) {
    var question = answer.populate('question').question;
    return {
      question: question.title,
      selectedOption: answer.selectedOption,
      answerText: question.options[answer.selectedOption],
      selectedNA: answer.selectedEmotion
    }
  });

  this.questionAnswersAggr = this.getQuestionsAndAnswers;
};

submissionSchema.methods.getQuestionsAndAnswers = function getQuestionsAndAnswers() {
  var questions = _.pluck(this.optionsSaved, 'question');
  var answers = _.pluck(this.optionsSaved, 'selectedOption');

  return {"questions": questions, "answers": answers};
};

module.exports = mongoose.model('Submission', submissionSchema);

//Listeners
var Submission = mongoose.model('Submission');

Submission.on('beforeSave', function (submission) {
  var Survey = mongoose.model('Survey');

  Survey.findOne({_id: submission.survey}, function (err, survey) {
    survey.getProgress();
  });

  submission.processAnswers();
});
