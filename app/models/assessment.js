'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    config = require('../../config/config.js');

//Loading constants
require(config.root + '/config/constants');

var fields = {
  tag: {
    type: String,
    enum: constants['tag_names']
  },
  questions: [],
  comment_questions: [],
  created_at: {
    type: Date,
    default: Date.now
  },
  survey: {
    type: Schema.Types.ObjectId,
    ref: 'Survey'
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
};

var assessmentSchema = new Schema(fields);

assessmentSchema.statics.buttonText = function buttonText(assessment) {
  var text = '';

  switch (assessment.tag) {
    case 'THRIVING':
      text = constants['thrivingButtons'];
      break;
    case 'TEAM EFFECTIVENESS':
      text = constants['teButtons'];
      break;
    case 'MANAGER EFFECTIVENESS':
      text = constants['meButtons'];
      break;
    default:
      text = constants['skillButtons'];
  }

  return text;
};

module.exports = mongoose.model('Assessment', assessmentSchema);
