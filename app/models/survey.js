'use strict';

var mongoose = require('mongoose'),
    _ = require('underscore'),
    Schema = mongoose.Schema;

var fields = {
  assessments: [],
  assignees: [],
  raters: [],
  name: {
    type: String
  },
  state: {
    type: String,
    enum: ['started', 'completed']
  },
  overallProgress: {
    type: Number,
    default: 0
  },created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
};

var surveySchema = new Schema(fields);

surveySchema.methods.getProgress = function getProgress(){
  var _this = this;
  var Submission = mongoose.model('Submission');

  Submission.find({survey: this._id}).exec(function(err, submissions){
    var submissions_progress = _.map(submissions, function(submission){
      return submission.getProgress();
    });

    var sum = _.reduce(submissions_progress, function(memo, num){ return memo + num; }, 0);
    _this.overallProgress = sum/submissions.length;

    _this.save(function(err, doc){
      if(!err){
        console.log('survey progress updated');
      }
    });
  })
};

surveySchema.methods.getQuestionsAndAnswers = function getQuestionsAndAnswers() {
  var questions = _.pluck(this.optionsSaved, 'question');
  var answers = _.pluck(this.optionsSaved, 'selectedOption');

  return {"questions": questions, "answers": answers};
};

surveySchema.methods.surveyReminder = function surveyReminder(sender, notifier){
  var Notification = mongoose.model('Notification');
  var newNotification = new Notification();

  newNotification.text = 'Submit Survey';
  newNotification.url = '/notifications/' + newNotification._id;
  newNotification.notifierId = notifier._id;
  newNotification.type = {
    initiatorId: sender._id,
    notifierId: notifier._id,
    typeId: this._id,
    notificationType: 'SurveyReminder'
  };

  newNotification.save(function (err, notification) {
    console.log('Notification Placed successfully -> ' + notification._id);
  });
};

surveySchema.methods.submissionNotificationRead = function submissionNotificationRead(userId){
  var Notification = mongoose.model('Notification');

  Notification.findOne({'type.notificationType': 'Survey', notifierId: userId, 'type.typeId': this._id},
      function (err, notification) {

        if (notification) {
//          Survey Reminder Notification
//          notification.readNotification();
        }
      })
};

module.exports = mongoose.model('Survey', surveySchema);