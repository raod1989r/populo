var _ = require('underscore'),
    request = require('request'),
    fs = require('fs'),
    rl = require('readline'),
    http = require('http');

require('../config/constants');

var userXlsx = function () {

  var formData = {
    userExcel: fs.createReadStream('./seeds/excels/pos.xlsx')
  };

  request.post(
      {url:'http://localhost:3000/xlsx/users',
        formData: formData,
        headers: {'apiKey': constants.apiKey}},
      function(err, httpResponse, body) {
    if (err) {
      console.error('upload failed:', err);
    }
    console.log('Upload successful!');
  });
};

var behaviorXlsx = function () {
  var formData = {
    behaviorsExcel: fs.createReadStream('./seeds/excels/behaviors.xlsx')
  };

  request.post(
      {url:'http://localhost:3000/xlsx/behaviors',
        formData: formData,
        headers: {'apiKey': constants.apiKey}},
      function(err, httpResponse, body) {
        if (err) {
          console.error('upload failed:', err);
        }
        console.log('Upload successful!');
      });
};

var inputSelection = function () {
  var prompts = rl.createInterface(process.stdin, process.stdout);

  console.log('1. User Excel imports');
  console.log('2. Behavior Excel imports');

  prompts.question('Select Any one of the option ? ', function (option) {
    switch (option) {
      case '1':
        userXlsx();
        break;
      case '2':
        behaviorXlsx();
        break;
      default:
        console.log('invalid option');
    }
    prompts.close();
  });
}();
