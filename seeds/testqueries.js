var express = require('express'),
    config = require('../config/config.js'),
    glob = require('glob'),
    _ = require('underscore'),
    fs = require('fs'),
    mongoose = require('mongoose');

//Loading constants
require(config.root + '/config/constants');

//Loading Models
var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

//MongoDB Connection
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var userBehavior = mongoose.model('UserBehavior');

var behaviorsList = function behaviorsList() {
  userBehavior.find({}, function (err, behaviors) {
    console.log(behaviors);
  });
};

var survey = mongoose.model('Survey');

var surveyList = function surveyList() {
  survey.find({}, function (err, surveys) {
    console.log(surveys);
  });
};

var user = mongoose.model('User');

var usersList = function usersList() {
  user.find({}, function (err, users) {
    var employees = _.filter(users, function (user) {
      return (user.role == 'Employee')
    });

    console.log(_.pluck(employees, 'role'));
  });
};

var Notification = mongoose.model('Notification');

var surveyRemindersDelete = function surveyRemindersDelete() {
  Notification.remove({'type.notificationType': 'SurveyReminder'},
      function (err, notifications) {

        if (!err) {
          console.log('successfully deleted');
        }
      })
}();