var express = require('express'),
    config = require('../config/config.js'),
    glob = require('glob'),
    _ = require('underscore'),
    mongoose = require('mongoose');

//Loading Models
var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

//MongoDB Connection
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var seedUsers = function () {
  console.log('Generating Users ....');
  var User = mongoose.model('User'),
      newUser = new User();
//      userEmail = 'admin@example.com';

  newUser.full_legal_name = 'admin';
  newUser.employee_id = '0000';

  User.remove().exec(function () {
    User.registerUser(newUser, 'Admin');
  });
}();

var generateUsers = function () {
  var users = ['user1', 'user2', 'user3'];
  var hrs = ['hr1', 'hr2', 'hr3'];
  var managers = ['manager1', 'manager2', 'manager3'];
  var User = mongoose.model('User');

  _.each(users, function (userName, index) {
    var newUser = new User();

    newUser.full_legal_name = userName;
    newUser.employee_id = '001'+index;

    User.registerUser(newUser, 'Employee');
  });

  _.each(hrs, function (hrName, index) {
    var newUser = new User();

    newUser.full_legal_name = hrName;
    newUser.employee_id = '002'+index;

    User.registerUser(newUser, 'HR Manager');
  });

  _.each(managers, function (managerName, index) {
    var newUser = new User();

    newUser.full_legal_name = managerName;
    newUser.employee_id = '002'+index;

    User.registerUser(newUser, 'Manager');
  });
}();
