var express = require('express'),
    config = require('../config/config.js'),
    glob = require('glob'),
    _ = require('underscore'),
    fs = require('fs'),
    xml2js = require('xml2js'),
    base64 = require('node-base64-image'),
    mongoose = require('mongoose');

//Loading constants
require(config.root + '/config/constants');

//Loading Models
var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

//MongoDB Connection
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var createSurvey = function () {
  console.log('Create Survey...');
  var Survey = mongoose.model('Survey');
  var Submission = mongoose.model('Submission');
  var Notification = mongoose.model('Notification');
  var User = mongoose.model('User');

  Survey.remove().exec(function () {
    console.log('Removing Surveys...');
  });

  Submission.remove().exec(function () {
    console.log('Removing Submissions...');
  });

  Notification.remove().exec(function () {
    console.log('Removing Notifications...');
  });

  User.find({}, function (err, users) {
    var newSurvey = new Survey;
    newSurvey.raters = users;
    newSurvey.assignees = users;
    newSurvey.state = 'started';

    newSurvey.save(function (err, survey) {
      if (err) {
        console.log('Error deploying Survey');
        throw 'Survey Deployment Error';
      }

      console.log('Deployed Surveys');
//        Notification.remove().exec();

      _.each(users, function (user) {
        var newNotification = new Notification();

        newNotification.text = 'Click here to complete your growth survey';
        newNotification.url = '/notifications/' + newNotification._id;
        newNotification.notifierId = user._id;
        newNotification.read = false;
        newNotification.type = {
          initiatorId: user._id,
          notifierId: user._id,
          typeId: newSurvey._id,
          notificationType: 'Survey'
        };

        newNotification.save(function (err, notification) {
          console.log('Notification Placed successfully -> ' + notification._id);
        });
      });

      seedAssessment(survey);
    });
  })
}();

var seedAssessment = function (survey) {
  console.log('Generate Assessments ...');
  var Assessment = mongoose.model('Assessment');
  var Survey = mongoose.model('Survey');
  var Question = mongoose.model('Question');

  Assessment.remove().exec(function () {
    console.log('Removing Assessments');
  });

  Question.remove().exec(function () {
    console.log('Removing Questions');
  });

  var assessments = _.map(constants['tag_names'], function (tag) {
    var newAssessment = new Assessment();
    newAssessment.tag = tag;
    newAssessment.survey = survey;
    newAssessment.questions = seedQuestions(newAssessment);
    newAssessment.comment_questions = seedComments(newAssessment);

    newAssessment.save();
    return newAssessment;
  });
  survey.assessments = assessments;
  survey.name = '2016 mid year peak survey';
  survey.save();
};

var seedQuestions = function (assessment) {

  //Generate Fake Question Data
  var thriving_questions = ['I feel I can make a difference here.',
    'When I look at what we accomplish, I feel a sense of pride.',
    'I am looking forward to what each new day brings.',
    'I see myself continuously learning and improving.',
    'I can aspire to be my best self at work.',
    'I am satisfied with the balance between work and personal life.'];

  var team_effectiveness_questions = [
    'Trust: My team is able to be vulnerable with one another, willing to admit their mistakes, acknowledge their weaknesses, and ask for help without hesitation.',
    'Healthy Conflict: My team challenges each other and understands that questioning the status quo is important to come to the best outcome.',
    'Commitment: My team is clear on objectives, aligned on priorities, and supports decisions made and plans of action even if there was initial disagreement.',
    'Accountability: My team consistently follows through on promises and commitments.',
    'Results: My team consistently achieves its objectives and has a reputation for high performance.'
  ];

  var manager_effectiveness_questions = [
    'My manager informs the team about important issues and changes.',
    'My manager communicates clear goals for our team.',
    'My manager keeps the team focused on our priority results/deliverables.',
    'My manager shows consideration for me as a person.',
    'My manager gives me actionable feedback that helps me improve my performance.',
    'My manager has positively influenced my growth and development.',
    'My manager shows appreciation for good work and extra effort.'
  ];

  var skill_questions = [
    'Attracting, identifying, and evaluating talent',
    'Coaching', 'Training design and facilitation', 'Relationship building (internal and external)',
    'Service oriented mentality', 'Business partner', 'Understanding of IMC business', 'External market/competitor knowledge',
    'Knowledge of psychology and its application', 'Using data/metrics to drive change', 'Knowledge of HR systems (workday/recsolu)',
    'Knowledge of employment law (cobra, FLMA, SOX)'
  ];

  var general_comment_questions = ['List 1-2 things you’d like your manager to continue doing', 'List 1-2 things you’d like your manager to start/stop doing'];

  var manager_effectiveness_comment_questions = general_comment_questions.push('Please provide comments or examples here. These comments are valuable as they make the feedback more actionable.');

  console.log('Generate Questions...');
  var Question = mongoose.model('Question');

  var questions = [];
  var options = [];

  switch (assessment.tag) {
    case 'BEHAVIORS':
      console.log('Skiping: Assessment type is Behaviour...');
      break;
    case 'SKILLS':
      questions = skill_questions;
      options = constants['skill_question_options'];
      break;
    case 'SKILLS1':
      questions = skill_questions;
      options = constants['skill_question_options'];
      break;
    case 'MANAGER EFFECTIVENESS':
      questions = manager_effectiveness_questions;
      options = constants['question_options'];
      break;
    case 'THRIVING':
      questions = thriving_questions;
      options = constants['question_options'];
      break;
    case 'TEAM EFFECTIVENESS':
      questions = team_effectiveness_questions;
      options = constants['question_options'];
      break;
  }

  var questionList = _.map(questions, function (question) {
    var newQuestion = new Question();
    newQuestion.title = question;
    newQuestion.not_able_to_judgement = false;
    newQuestion.options = options;

    newQuestion.save();
    return newQuestion;
  });

  return questionList;
};

var seedComments = function (assessment) {
  var general_comment_questions = ['Please provide comments or examples here. These comments are valuable as they ' +
      'make the feedback more actionable.'];
  var manager_effectiveness_comment_questions = ['List 1-2 things you’d like your manager to continue doing',
    'List 1-2 things you’d like your manager to start/stop doing',
    'Please provide comments or examples here. These comments are valuable as they ' +
        'make the feedback more actionable.'];

  console.log('Generate Questions...');
  var Question = mongoose.model('Question');

  var comments = [];

  switch (assessment.tag) {
    case 'BEHAVIORS':
      console.log('Skiping: Assessment type is Behaviour...');
      break;
    case 'SKILLS':
      comments = general_comment_questions;
      break;
    case 'SKILLS1':
      comments = general_comment_questions;
      break;
    case 'MANAGER EFFECTIVENESS':
      comments = manager_effectiveness_comment_questions;
      break;
    case 'THRIVING':
      comments = general_comment_questions;
      break;
    case 'TEAM EFFECTIVENESS':
      comments = general_comment_questions;
      break;
  }

  var commentsList = _.map(comments, function (comment) {
    var newQuestion = new Question();
    newQuestion.title = comment;

    newQuestion.save();
    return newQuestion;
  });

  return commentsList;
};

var generatePhotos = function () {
  var parser = new xml2js.Parser();
  var User = mongoose.model('User');

  fs.readFile(__dirname + '/photos-xml/worker_foto.xml', function (err, data) {
    parser.parseString(data, function (err, result) {

      var base64Data = result['wd:Report_Data']['wd:Report_Entry'][0]['wd:Photo'][0]['wd:Base64_Image_Data'][0];
      var name = result['wd:Report_Data']['wd:Report_Entry'][0]['wd:Name'][0];
      var localFilePath = __dirname + '/../public/avatar/' + name;
      var relativePath = '/files/avatar/' + name + '.jpg';

      base64.base64decoder(base64Data, {filename: localFilePath}, function (err, image) {
        if (err) {
          console.log(err);
        } else {
          User.find({}, function (err, users) {
            _.each(users, function (user) {
              user.avatar = relativePath;
              user.save();
            })
          })
        }
      });
    });
  });
};

generatePhotos();