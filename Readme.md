## __Read Me__
----------------------

## Installation Notes
  * Installed NPM, NodeJS(v 4.4.3) and MongoDB Softwares
  * git clone "repo_url" 
  * On root directory 
     1. npm install
     2. grunt (to start server)

## Seed/ Faker Tasks
  * node seeds/seed.js
  * node seeds/faker.js
  
  
## Debug Mode
  * devtool app.js --watch
  

## Code Standards
    jshint <file_loc>

## Schema Generators
    yo mongoose:schema "article|title:String,excerpt:String,content:String,published:Boolean,created:Date"
