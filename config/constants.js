constants = {
  apiKey: '1a965aa09e1d8e5f924942b168787233',
  tag_names: ['THRIVING', 'TEAM EFFECTIVENESS',
    'MANAGER EFFECTIVENESS', 'BEHAVIORS', 'SKILLS'],
  human_tag_names: ['Behaviors', 'Manager Effectiveness', 'Skills', 'Team Effectiveness', 'Thriving'],
  question_options: ['Not at all', 'Not really', 'Neutral', 'Yes', 'Enthusiastic yes'],
  skill_question_options: ['Significant growth needed', 'Some growth needed',
    'Adequately exhibits this skill', 'Exemplifies', 'Exemplifies and successfully grows it in others'],

  <!--Thriving-->
  thrivingButtons: "\
                      <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='We define thriving as learning and vitality - the energy and motivation that you bring each day to work. We believe that your thriving will lead to better results for you and the organization. In planning for your growth, we ask you to consider how you might increase your learning and vitality. Our measurement of thriving is informed by the work of Gretchen Spreitzer, author of A Socially Embedded Model of Thriving at Work, and Paul McGinniss, a Neuroscientist at the NeuroLeadership Institute. '>why we measure thriving </button> \
                      <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='To measure thriving we use a custom five item, level of agreement scale. Our objective is to understand the degree to which you agree with each statement. The scale is: Not at all, Not really, Neutral, Yes, Enthusiastic yes. You can also select Not able to make a judgment.'>what the measures mean </button> \
                      <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='The organization will see aggregate responses by function. Your manager will see your responses.You will see your responses. '>who will see what</button> \
                    ",

  <!--Team Effectiveness -->
  teButtons: "\
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='Cohesive and effective teams outperform ineffective teams. Having more effective teams at IMC Chicago will help us to achieve our business goals. We believe the five behavioral principles listed below contribute to team effectiveness. It takes considerable time and effort to develop a successful, high performing team. In planning for your growth, we ask you to consider how you might contribute to your team's effectiveness.Our measurement of team effectiveness is informed by the work of Patrick Lencioni, author of The Advantage and Five Dysfunctions of a Team: A leadership fable. '>why we measure team effectiveness</button> \
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='To measure team effectiveness we use a custom five item, level of agreement scale. Our objective is to understand the degree to which you agree with each statement. The scale is: Not at all, Not really, Neutral, Yes, Enthusiastic yes. You can also select Not able to make a judgment. '>what the measures mean</button> \
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='Your manager&#8216;s manager (leader of function) will see responses aggregated for your function. Your manager will see your responses and your team&#8216;s responses. Your team will see an aggregate of team responses. You will see your responses and an aggregate of your team responses.'>who will see what </button> \
              ",

  <!--Manager Effectiveness -->
  meButtons: "\
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='Manager effectiveness is directly correlated to employee satisfaction and engagement. The more effective our managers, the better our people and teams will be. We believe the six areas listed below measure manager effectiveness. In planning for your growth, we ask you to consider how you might contribute to your manager&#8216;s effectiveness. Our measurement of manager effectiveness is informed by the Laszlo Bock, author of Work Rules: Insights from Inside Google, the Great Place to Work Survey, and Paul McGinniss, a Neuroscientist at the NeuroLeadership Institute. '>why we measure manager effectiveness</button> \
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='To measure manager effectiveness we use a custom five item, level of agreement scale. Our objective is to understand the degree to which you agree with each statement. The scale is: Not at all, Not really, Neutral, Yes, Enthusiastic yes. You can also select Not able to make a judgment. '>what the measures mean</button> \
                <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='Your manager&#8216;s manager (leader of function) will see the aggregate responses by function. Your manager will NOT see your individual responses. S/he will only see an aggregate response from your team members. You will see your responses. '>who will see what </button> \
              ",


  <!--Skills -->
  skillButtons: "\
                    <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='The skills listed below are necessary to be successful in your function at IMC Chicago. In planning for your growth, we ask you to consider how you might better exemplify these skills. These skills are applicable to and measured ONLY for your function.'>why we measure skills</button> \
                    <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='To measure skills we use a custom five item, growth scale. Our objective is to understand what degree of growth an individual should plan for in each skill. The scale is: Significant growth needed in this skill, Some growth needed in this skill, Adequately exhibits this skill, Exemplifies this skill, Exemplifies this skill and successfully grows it in others, You can also select Not able to make a judgment. Key Terms: Exemplify: To serve as an example of. Adequate: As much or as good as necessary. '>what the measures mean</button> \
                    <button class='btn btn-st text-capitalize' type='button' data-toggle='tooltip' data-placement='top' title='Your manager&#8216;s manager (leader of function) will see responses aggregated for your function. Your manager will see your individual responses and your team&#8216;s responses. You will see your responses, the responses your manager provided for you and the responses your team provided for you. '>who will see what </button> \
                  "

}
