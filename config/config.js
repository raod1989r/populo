var path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  env = process.env.NODE_ENV || 'staging';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'populo'
    },
    SMTPUsername: 'dev.populo',
    SMTPPassword: 'testtest1',
    port: 3000,
    db: 'mongodb://localhost/populo-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'populo'
    },
    SMTPUsername: '',
    SMTPPassword: '',
    port: 3000,
    db: 'mongodb://populo:populo@ds039145.mlab.com:39145/populo-test'
  },

  staging: {
    root: rootPath,
    app: {
      name: 'populo-staging'
    },
    SMTPUsername: 'dev.populo',
    SMTPPassword: 'testtest1',
    port: 3000,
    db: 'mongodb://populo_assessment:Populo_Assessment@52.48.79.171/assessment_staging'
  },

  production: {
    root: rootPath,
    app: {
      name: 'populo-production'
    },
    SMTPUsername: 'dev.populo',
    SMTPPassword: 'testtest1',
    port: 3000,
    db: 'mongodb://populo_assessment:Populo_Assessment@52.48.79.171/assessment_prod'
  }
};

module.exports = config[env];
