var express = require('express'),
    config = require('./config/config'),
    glob = require('glob'),
    passport = require('passport'),
    expressSession = require('express-session'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    nodemailer = require('nodemailer'),
    flash = require('connect-flash');

//MongoDB Connection
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

//Session configurations
var app = express();
app.use(expressSession({secret: 'mySecretKey'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 100000}));
app.use('/files', express.static(__dirname + '/public'));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//SMTP Config
app.smtpTransport = nodemailer.createTransport('smtps://' + config.SMTPUsername + ':' + config.SMTPPassword + '@smtp.gmail.com');

//Underscore Helpers
app.locals._ = require("underscore");

//Loading Models
var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function (model) {
  require(model);
});

// Give Views/Layouts direct access to session data.
app.use(function (req, res, next) {
  res.locals.user = req.user;
  next();
});

require('./config/express')(app, config);
require('./config/passport');

//Starting Server
app.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});
